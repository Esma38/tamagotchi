
//on cible le bouton MANGER
let btnManger = document.getElementById('manger');
//on cible la bare FAIM
let progressBarFaim = document.getElementById('faim');

//on cible le bouton JOUER
let btnJouer = document.getElementById('jouer');
//on cible la bare JOIE
let progressBarJoie = document.getElementById('humeur');

//on cible le bouton SE LAVER
let btnSeLaver = document.getElementById('se-laver');
//on cible la bare Propreté
let progressBarProprete = document.getElementById('hygiene');

//on cible le bouton DORMIR
let btnDormir = document.getElementById('dormir');
//on cible la bare Energie
let progressBarEnergie = document.getElementById('energie');

//on cible la console
let pandaConsole = document.getElementById('console');


/*   incrementation FAIM */
//on prepare la fonction qui sera attachee a notre evenement click sur le bouton MANGER
//elle fera la decrementation de la barre faim

function manger(){
    console.log(progressBarFaim);
    //on recupere la valeur de l'atribut 'aria-valuenow'
    let val = parseInt(progressBarFaim.getAttribute('aria-valuenow'));
    //tant que cette valeur est < 100, on l'incremente (+5)
    if (val < 100) {
        let increment = val + 5;
        //on met a jour l'atribut 'aria-valuenow' et la width de la barre
        progressBarFaim.setAttribute('aria-valuenow', increment);
        progressBarFaim.style.width = increment + "%";
        toastr.success("Excellent, Ton panda a mangé! ");
    }else
    {
        toastr.success("Excellent, Ton panda n'a plus faim!");
    }
}
//on ajoute un EventListener au click sur le bouton MANGER
btnManger.addEventListener('click', manger);

/* decrementation FAIM */
function decrementFaim(){
    //on recupere les memes elements comme pour l'incrementation
    let val = parseInt(progressBarFaim.getAttribute('aria-valuenow'));
    if (val > 0) {
        let decrement = val - 5;
        //on met a jour l'atribut 'aria-valuenow' et la width de la barre
        progressBarFaim.setAttribute('aria-valuenow', decrement);
        progressBarFaim.style.width = decrement + "%";
        toastr.warning ("Attention, Ton panda à un creux! ");
    }else
    {
        toastr.error ("Alerte, Ton panda meurt de faim! ");
    }
}
let intervalFaim =  setInterval(decrementFaim, 2000);


/*   incrementation JOIE*/
//on ajoute un EventListener au click sur le bouton JOUER
function jouer(){
    console.log(progressBarJoie);
    //on recupere la valeur de l'atribut 'aria-valuenow'
    let val = parseInt(progressBarJoie.getAttribute('aria-valuenow'));
    console.log(val);
    //autant que cette valeur est < 100, on l'incremente (+5)
    if (val < 100) {
        let increment = val + 5;
        //on met a jour l'atribute 'aria-valuenow' et la width de la barre
        progressBarJoie.setAttribute('aria-valuenow', increment);
        progressBarJoie.style.width = increment + "%";
        toastr.success("Excellent, Ton panda a joué! ");
    }else
    {
        toastr.success("Excellent, Ton panda ne veut plus jouer!");
    }
}
btnJouer.addEventListener('click', jouer);

/* decrementation JOIE */
function decrementJoie(){
    //on recupere les memes elements comme pour l'incrementation
    let val = parseInt(progressBarJoie.getAttribute('aria-valuenow'));
    console.log(val);
    if (val > 0) {
        let increment = val - 5;
        //on met a jour l'atribut 'aria-valuenow' et la width de la barre
        progressBarJoie.setAttribute('aria-valuenow', increment);
        progressBarJoie.style.width = increment + "%";
        toastr.warning ("Attention, Ton panda s'ennuie! ");
    }else
    {
        toastr.error ("Alerte, Ton panda meurt d'ennuis! ");
    }
}
let intervalJoie =  setInterval(decrementJoie, 2000);


/*   incrementation Propreté*/
//on ajoute un EventListener au click sur le bouton SE LAVER
function seLaver(){
    console.log(progressBarProprete);
    //on recupere la valeur de l'atribut 'aria-valuenow'
    let val = parseInt(progressBarProprete.getAttribute('aria-valuenow'));
    console.log(val);
    //autant que cette valeur est < 100, on l'incremente (+5)
    if (val < 100) {
        let increment = val + 5;
        //on met a jour l'atribut 'aria-valuenow' et la width de la barre
        progressBarProprete.setAttribute('aria-valuenow', increment);
        progressBarProprete.style.width = increment + "%";
        toastr.success("Excellent, Ton panda se lave! ");
    }else
    {
        toastr.success("Excellent, Ton panda est propre!");
    }
}
btnSeLaver.addEventListener('click', seLaver);

/* decrementation Propreté */

let intervalProprete =  setInterval(function(){
    //on recupere les memes elements comme pour l'incrementation
    let val = parseInt(progressBarProprete.getAttribute('aria-valuenow'));
    console.log(val);
    if (val > 0) {
        let increment = val - 5;
       //on met a jour l'atribut 'aria-valuenow' et la width de la barre
        progressBarProprete.setAttribute('aria-valuenow', increment);
        progressBarProprete.style.width = increment + "%";
        toastr.warning ("Attention, Ton panda sent mauvais! ");
    }else
    {
        toastr.error ("Alerte, Ton panda est très sale! ");
    }
}, 2000);


/*   incrementation ENERGIE */
//on ajoute un EventListener au click sur le bouton DORMIR
function dormir(){
    console.log(progressBarEnergie);
    //on recupere la valeur de l'atribute 'aria-valuenow'
    let val = parseInt(progressBarEnergie.getAttribute('aria-valuenow'));
    console.log(val);
    //tant que cette valeur est < 100, on l'incremente (+5)
    if (val < 100) {
        let increment = val + 5;
        //on met a jour l'atribut 'aria-valuenow' et la width de la barre
        progressBarEnergie.setAttribute('aria-valuenow', increment);
        progressBarEnergie.style.width = increment + "%";
        toastr.success("Excellent, Ton panda a bien dormi! ");
    }else
    {
        toastr.success("Excellent, Ton panda n'a plus besoin de dormir!");
    }
}
btnDormir.addEventListener('click', dormir);

/* decrementation ENERGIE */

let intervalEnergie =  setInterval(function(){
    //on recupere les memes elements comme pour l'incrementation
    let val = parseInt(progressBarEnergie.getAttribute('aria-valuenow'));
    console.log(val);
    if (val > 0) {
        let increment = val - 5;
       //on met a jour l'atribut 'aria-valuenow' et la width de la barre
        progressBarEnergie.setAttribute('aria-valuenow', increment);
        progressBarEnergie.style.width = increment + "%";
        toastr.warning ("Attention, Ton panda a sommeil! ");
    }else
    {
        toastr.error ("Alerte, Ton panda meurt de fatigue! ");
    }
}, 2000);

//on ajoute un listener pour l'evenement keypress
pandaConsole.addEventListener('keypress', function readConsole (event) {
    //quand l'utilisateur tape Enter, alors on recupere le message qui est dans la console
    //on sait que la cle Enter a ete tape parce que le keyCode de l'evenement est 13
    if (event.keyCode == 13) {
        //on recupere le message qui est dans la console, dans le champ input
        let cmd = pandaConsole.value;
        console.log(cmd);
        //on fait ce que correspond a chaque commande
        switch (cmd) {
            case 'panda m':
                manger();
                break;
            case 'panda j':
                jouer();
                break;
            case 'panda l':
                seLaver();
                break;
            case 'panda d':
                dormir();
                break;
            default: //si l'utilisateur introduit autre chose, on affiche une notification
                console.log("Commande inconnue");
                toastr.error("Commande inconnue, Lissez le dictionnaire! ");
        }
    }
});